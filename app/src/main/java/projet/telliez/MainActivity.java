package projet.telliez;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import static projet.telliez.ToastMaker.toast;

/**
 * The type Main activity.
 */
public class MainActivity extends AppCompatActivity {

    /**
     *  Enum for swap test mode
     */
    private enum TestMode { DEFAULT_MODE, TEST_MODE }

    private MenuItem log_item;
    private MenuItem admin_items;
    private MenuItem get_palindrome_item;
    private MenuItem get_sentence_item;

    private EditText init_editText;
    private TextView normalized_text;
    private TextView flip_text;
    private Button clean_button;
    private Button flip_button;
    private Button compare_button;

    private ProgressBar progress_bar;

    private String random_palindrome;
    private String normalized_palindrome;
    private String flip_palindrome;
    private ArrayList<String> list_palindromes;
    private ArrayList<String> list_sentences;
    private ArrayList<String> default_list_palindromes;
    private ArrayList<String> default_list_sentences;

    private StringManager stringManager;

    private FileManager fileManager;
    private Random random = new Random();
    private String file_sentences = "file_sentences.txt";
    private String file_palindromes = "file_palindromes.txt";

    private boolean isCompared = false;
    private boolean isLogged = false;
    private boolean isEmptyListPalindrome = false;
    private boolean isEmptyListSentence = false;

    private TestMode mode = TestMode.DEFAULT_MODE;

    public static final int LOGIN = 1;
    public static final int ADD_SENTENCE = 2;
    public static final int ADD_PALINDROME = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initManager();
        initLists();

        clean_button.setOnClickListener(v -> cleanPalindrome() );
        flip_button.setOnClickListener(v -> flipPalindrome() );
        compare_button.setOnClickListener(v -> comparePalindrome() );

        init_editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { reset();}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { random_palindrome += s; }
            @Override
            public void afterTextChanged(Editable s) {
                random_palindrome = s.toString();
            }
        });
    }

    private void initManager() {
        stringManager = new StringManager(this, getColor(R.color.green), getColor(R.color.red), progress_bar);
        fileManager = new FileManager(this);
    }

    private void initViews() {
        init_editText = findViewById(R.id.init_editText);
        normalized_text = findViewById(R.id.normalized_text);
        clean_button = findViewById(R.id.clean_button);
        flip_button = findViewById(R.id.flip_button);
        compare_button = findViewById(R.id.compare_button);
        flip_text = findViewById(R.id.flip_text);
        progress_bar = findViewById(R.id.progress_bar);
    }

    private void initLists() {
        list_palindromes = new ArrayList<>();
        list_sentences = new ArrayList<>();
        default_list_palindromes = new ArrayList<>();
        default_list_sentences = new ArrayList<>();

        list_palindromes.addAll(fileManager.readFile(file_palindromes));
        list_sentences.addAll(fileManager.readFile(file_sentences));
        isEmptyListPalindrome = list_palindromes.isEmpty();
        isEmptyListSentence = list_sentences.isEmpty();
        default_list_palindromes.addAll(fileManager.readRaw(getResources().openRawResource(R.raw.palindromes)));
        default_list_sentences.addAll(fileManager.readRaw(getResources().openRawResource(R.raw.non_palindromes)));
    }

    /**
     * Check if string is null or empty
     *
     * @param to_check
     * @param msg string to display with a toast
     * @return boolean
     */
    private boolean checkEmpty(String to_check, String msg) {
        if(to_check == null || to_check.equals("")){
            toast(msg,this);
            return true;
        }
        return false;
    }

    /**
     * Display a toast if boolean isCompared is true or not
     *
     * @param msg string to display with a toast
     * @return boolean
     */
    private boolean checkIsCompared(String msg) {
        if(isCompared) {
            toast(msg, this);
        }
        return isCompared;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        initItems(menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void initItems(Menu menu) {
        log_item = menu.findItem(R.id.login_item);
        admin_items = menu.findItem(R.id.admin_items);
        get_palindrome_item = menu.findItem(R.id.get_palindrome_item);
        get_sentence_item = menu.findItem(R.id.get_sentence_item);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.random_palinidrom_item:
                getAllPalindrome();
                updateEditText();
                return true;
            case R.id.random_sentence_item:
                getAllSentence();
                updateEditText();
                return true;
            case R.id.about_item:
                showFragment();
                return true;
            case R.id.login_item:
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivityForResult(intent,LOGIN);
                return true;
            case R.id.test_item:
                updateActivityForTestMode();
                reset();
                init_editText.setText("");
                return true;
            case R.id.add_sentence_item:
                Intent intent_sentence = new Intent(MainActivity.this, AddActivity.class);
                intent_sentence.putExtra("add",ADD_SENTENCE);
                startActivityForResult(intent_sentence, ADD_SENTENCE);
                return true;
            case R.id.add_palindrome_item:
                Intent intent_palindrome = new Intent(MainActivity.this, AddActivity.class);
                intent_palindrome.putExtra("add",ADD_PALINDROME);
                startActivityForResult(intent_palindrome, ADD_PALINDROME);
                return true;
            case R.id.get_palindrome_item:
                random_palindrome = getRandomPalindrome();
                updateEditText();
                return true;
            case R.id.get_sentence_item:
                random_palindrome = getRandomSentence();
                updateEditText();
                return true;
            case R.id.logout_item:
                logout();
                return true;

        }
        return false;
    }

    private void logout() {
        isLogged = false;
        showItems();
    }

    private void updateActivityForTestMode() {
        if ( mode == TestMode.DEFAULT_MODE) {
            mode = TestMode.TEST_MODE;
            flip_text.setVisibility(View.GONE);
            flip_button.setVisibility(View.GONE);

        } else if ( mode == TestMode.TEST_MODE) {
            mode = TestMode.DEFAULT_MODE;
            flip_text.setVisibility(View.VISIBLE);
            flip_button.setVisibility(View.VISIBLE);
        }
        toast("Changement du mode de comparaison", this);
    }

    private String getDefaultRandomPalindrome() {
        int rd_default = default_list_palindromes.size();
        return default_list_palindromes.get(random.nextInt(rd_default));
    }

    private String getRandomPalindrome() {
        int rd = list_palindromes.size();
        return list_palindromes.get(random.nextInt(rd));
    }

    private void getAllPalindrome() {
        reset();
        if( !isEmptyListPalindrome) {
            int nb = list_palindromes.size() + default_list_palindromes.size();
            random_palindrome = (random.nextInt(nb) >= nb / 4 ) ? getDefaultRandomPalindrome() : getRandomPalindrome();
        } else
            random_palindrome = getDefaultRandomPalindrome();
    }

    private String getRandomSentence() {
        int rd = list_palindromes.size();
        int rds = list_sentences.size();
        return (random.nextBoolean()) ? list_palindromes.get(random.nextInt(rd)) : list_sentences.get(random.nextInt(rds));
    }

    private String getDefaultRandomSentence() {
        int rd_default = default_list_palindromes.size();
        int rds_default = default_list_sentences.size();
        return (random.nextBoolean()) ? default_list_palindromes.get(random.nextInt(rd_default)) : default_list_sentences.get(random.nextInt(rds_default));
    }

    private void getAllSentence() {
        reset();
        int nb = list_palindromes.size() + default_list_palindromes.size() + list_sentences.size() + default_list_sentences.size();
        String default_sentence = (random.nextBoolean()) ? getDefaultRandomSentence() : getDefaultRandomPalindrome();
        if( !isEmptyListSentence){
            String sentence = (random.nextBoolean()) ? getRandomSentence() : getRandomPalindrome();
            random_palindrome = (random.nextInt(nb) >= nb / 4) ? default_sentence : sentence;
        } else
            random_palindrome = default_sentence;
    }

    private void updateEditText(){
        init_editText.setText(random_palindrome);
    }

    private void showFragment() {
        AboutFragment aboutFragment = new AboutFragment(this);
        aboutFragment.show();
    }

    private void cleanPalindrome() {
        if ( checkEmpty(random_palindrome, "Choisir un palindrome")) return;
        if ( checkIsCompared("Déjà nettoyer !")) return;
        normalized_text.setText( normalized_palindrome = stringManager.cleanString(random_palindrome));
    }

    private void flipPalindrome() {
        if ( checkEmpty(normalized_palindrome, "Veuillez nettoyer le palindrome !")) return;
        if ( checkIsCompared("Déjà retourner !")) return;
        flip_text.setText(flip_palindrome = stringManager.flipString(normalized_palindrome));
    }

    private void comparePalindrome() {
        if ( checkEmpty(normalized_palindrome, "Veuillez nettoyer le palindrome !")) return ;
        if ( checkIsCompared("Déjà comparer !")) return;
        Thread thread = new Thread(() -> {
            switch(mode) {
                case DEFAULT_MODE: // Mode par defaut
                    defaultMode();
                    break;
                case TEST_MODE: // MODE TEST
                    modeTest();
                    break;
            }
            isCompared = true;
        });
        thread.start();
    }

    private void defaultMode()  {
        try { stringManager.defaultCompareMode(normalized_palindrome, flip_palindrome, normalized_text, flip_text ); }
        catch (InterruptedException e) {e.printStackTrace();}
    }

    private void modeTest(){
        try {  stringManager.testCompareMode(normalized_palindrome,  normalized_text); }
        catch (InterruptedException e) {e.printStackTrace();}
    }

    private void reset() {
        random_palindrome = null;
        normalized_palindrome = null;
        flip_palindrome = null;
        normalized_text.setText("");
        flip_text.setText("");
        progress_bar.setProgress(0);
        isCompared = false;
    }

    private void showItems() {
        log_item.setVisible(!isLogged);
        admin_items.setVisible(isLogged);
    }

    private void showItemGetPalindrome(boolean visible) {
        get_palindrome_item.setVisible(visible);
    }
    private void showItemGetSentence(boolean visible) {
        get_sentence_item.setVisible(visible);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (resultCode) {
            case RESULT_CANCELED:
                break;
            case LOGIN:
                isLogged = data.getBooleanExtra("login", true);
                toast("Connexion" , this);
                showItems();
                showItemGetPalindrome(!isEmptyListPalindrome);
                showItemGetSentence(!isEmptyListSentence);
                break;
            case ADD_SENTENCE:
                String sentence = data.getStringExtra("sentence");
                list_sentences.add(sentence);
                fileManager.saveFile(list_sentences, file_sentences);
                showItemGetSentence(true);
                break;
            case ADD_PALINDROME:
                String palindrome = data.getStringExtra("palindrome");
                list_palindromes.add(palindrome);
                fileManager.saveFile(list_palindromes, file_palindromes);
                showItemGetPalindrome(true);
                break;
        }
    }
}