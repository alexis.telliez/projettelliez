package projet.telliez;

import static projet.telliez.MainActivity.LOGIN;
import static projet.telliez.ToastMaker.toast;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

/**
 * The type Login activity.
 */
public class LoginActivity extends AppCompatActivity {

    private EditText password_editText;
    private static String PASSWORD = "perec";

    private String password;

    private Button btn_cancel;
    private Button btn_login;

    private static int MAX_ATTEMPT = 3;
    private int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();

        btn_login.setOnClickListener(v-> login());
        btn_cancel.setOnClickListener(v-> cancel());

        password_editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { password += s; }
            @Override
            public void afterTextChanged(Editable s) {
                password = s.toString();
            }
        });
    }

    private void init() {
        counter = MAX_ATTEMPT;
        password_editText = findViewById(R.id.editText_password);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_login = findViewById(R.id.btn_login);
    }

    private void login() {
        if( password.isEmpty()) toast("Mot de passe vide !", this);
        else if(password.equals(PASSWORD)) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("login", true);
            setResult(LOGIN, intent);
            finish();
        } else {
            toast("Mauvais mot de passe ! Tentative(s) restante(s)" + counter, this);
            counter--;
            if( counter == 0) {
                btn_login.setEnabled(false);
                cancel();
            }
        }

    }

    private void cancel() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }
}