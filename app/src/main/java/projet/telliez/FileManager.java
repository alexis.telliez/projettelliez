package projet.telliez;

import static android.content.Context.MODE_PRIVATE;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

/**
 * The type File manager.
 */
public class FileManager {

    private Context context;

    /**
     * Instantiates a new File manager.
     *
     * @param context the context
     */
    public FileManager(Context context)  {
        this.context = context;
    }

    /**
     * Read raw array list.
     *
     * @param input_file the input file
     * @return the array list
     */
    public ArrayList<String> readRaw(InputStream input_file) {
        ArrayList<String> list_palindromes = new ArrayList<>();
        try {

            BufferedReader reader = new BufferedReader( new InputStreamReader(input_file));
            for (String line; (line = reader.readLine()) != null; )  list_palindromes.add(line);

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list_palindromes;
    }

    /**
     * Read file array list.
     *
     * @param fileName the file name
     * @return the array list
     */
    public ArrayList<String> readFile(String fileName)  {
        String value = "";
        ArrayList<String> values = new ArrayList<>();
        try {
            FileInputStream inputStream = context.openFileInput(fileName);
            BufferedReader bufferedReader =  new BufferedReader(new InputStreamReader(inputStream,"utf8"), 8192);
            while ((value=bufferedReader.readLine())!= null) values.add(value);

            bufferedReader.close();
            inputStream.close();
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            e.printStackTrace();
            return values;
        } catch (IOException e) {
            e.printStackTrace();
            return values;
        }
        return values;
    }

    /**
     * Save file.
     *
     * @param values   the values
     * @param fileName the file name
     */
    public void saveFile(ArrayList<String> values, String fileName) {
        try {
            FileOutputStream outputStream= context.openFileOutput(fileName, MODE_PRIVATE);
            OutputStreamWriter outWriter = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);

            for (int i = 0; i < values.size(); i++) outWriter.append(values.get(i)+"\n");

            outWriter.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
