package projet.telliez;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static projet.telliez.MainActivity.ADD_PALINDROME;
import static projet.telliez.MainActivity.ADD_SENTENCE;
import static projet.telliez.ToastMaker.toast;

/**
 * The type Add activity.
 */
public class AddActivity extends AppCompatActivity {

    private Button cancel_btn;
    private Button add_btn;

    private EditText add_editText;
    private TextView textView;

    private String sentence;

    private StringManager stringManager;

    private boolean isPalindrome = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        initStringManager();
        initViews();
        getMyIntent();

        cancel_btn.setOnClickListener( v -> cancel());
        add_editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { sentence += s; }
            @Override
            public void afterTextChanged(Editable s) {
                sentence = s.toString();
            }
        });

    }

    private void initViews() {
        cancel_btn = findViewById(R.id.cancel_add_btn);
        add_btn = findViewById(R.id.add_btn);
        add_editText = findViewById(R.id.add_sentence);
        textView = findViewById(R.id.text_add);
    }

    private void initStringManager() {
        stringManager = new StringManager();
    }

    private void getMyIntent() {
        Intent data = getIntent();
        int result = data.getIntExtra("add", ADD_SENTENCE);
        switch (result) {
            case ADD_SENTENCE:
                textView.setText(getResources().getText(R.string.add_sentence_text));
                add_btn.setOnClickListener(v -> add_sentence());
                break;
            case ADD_PALINDROME:
                textView.setText(getResources().getText(R.string.add_palindrome_text));
                add_btn.setOnClickListener(v -> add_palindrome());
                break;
        }
    }

    private void add_palindrome() {
        if(!check()) return;
        String copy = stringManager.cleanString(sentence);
        isPalindrome = stringManager.simpleCompareMode(copy);

        if(isPalindrome) {
            toast("Ajout d'un palindrome", this);
            Intent intent = new Intent(AddActivity.this, MainActivity.class);
            intent.putExtra("palindrome", sentence);
            setResult(ADD_PALINDROME, intent);
            finish();
        } else {
            toast("Ce n'est pas un palindrome", this);
            cancel();
        }
    }

    private void add_sentence() {
        if(!check()) return;
        Intent intent = new Intent(AddActivity.this, MainActivity.class);
        toast("Ajout d'une phrase", this);
        intent.putExtra("sentence", sentence);
        setResult(ADD_SENTENCE, intent);
        finish();
    }

    private boolean check() {
        if(sentence.isEmpty() || sentence.trim().length() == 0) {
            toast("Vous ne pouvez pas ajouter une phrase vide !", this);
            return false;
        }
        return true;
    }

    private void cancel() {
        Intent intent = new Intent(AddActivity.this, MainActivity.class);
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }
}