package projet.telliez;

import android.app.Activity;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.Normalizer;

/**
 * The type String manager.
 */
public class StringManager {

    private static final int TIME = 1500;
    private int green_color;
    private int red_color;
    private ProgressBar progress_bar;
    private Activity activity;
    private boolean isCompared = false;

    /**
     * Instantiates a new String manager.
     *
     * @param activity     the activity
     * @param green_color  the green color
     * @param red_color    the red color
     * @param progress_bar the progress bar
     */
    public StringManager(Activity activity, int green_color, int red_color, ProgressBar progress_bar) {
        this.activity = activity;
        this.green_color = green_color;
        this.red_color = red_color;
        this.progress_bar = progress_bar;
    }

    /**
     * Instantiates a new String manager.
     */
    public StringManager() {}

    /**
     * Clean string string.
     *
     * @param toClean the string to clean
     * @return the string
     */
    public String cleanString(String toClean) {
        return Normalizer.normalize(toClean, Normalizer.Form.NFD).replaceAll("[\\s-+.^'!?;§/*$£¤²:,]","").replaceAll("[^a-zA-Z]", "").toLowerCase();
    }

    /**
     * Flip string string.
     *
     * @param toFlip the to flip
     * @return the string
     */
    public String flipString(String toFlip) {
        StringBuilder sb = new StringBuilder();
        sb.append(toFlip);
        return sb.reverse().toString();
    }

    /**
     * Default compare mode.
     *
     * @param toCompare1 the first string to compare
     * @param toCompare2 the second string to compare
     * @param textView1  the first text view
     * @param textView2  the second text view
     * @throws InterruptedException the interrupted exception
     */
    public void defaultCompareMode(String toCompare1, String toCompare2, TextView textView1, TextView textView2) throws InterruptedException {
        isCompared = false;
        SpannableString normalized = new SpannableString(toCompare1), flip = new SpannableString(toCompare2);
        int size = toCompare1.length();
        int color = green_color;
        for (int i = 0; i < size;  i++) {
            if( toCompare1.charAt(i) != toCompare2.charAt(i)){
                color = red_color;
                isCompared = true;
            }

            normalized.setSpan(new BackgroundColorSpan(color), i, i + 1, 0);
            flip.setSpan(new BackgroundColorSpan(color), i, i + 1, 0);
            progress_bar.setProgress((i + 1) * 100/size);

            activity.runOnUiThread( () -> {
                textView1.setText(normalized);
                textView2.setText(flip);

            });

            if(isCompared) break;
            Thread.sleep(TIME/size);
        }
    }

    /**
     * Test compare mode.
     *
     * @param toCompare the to compare
     * @param textView1 the text view 1
     * @throws InterruptedException the interrupted exception
     */
    public void testCompareMode(String toCompare, TextView textView1) throws InterruptedException {
        isCompared = false;
        SpannableString normalized = new SpannableString(toCompare);
        int size = toCompare.length();
        int color = green_color;
        for (int i = 0; i < size / 2;  i++) {
            if( toCompare.charAt(i) != toCompare.charAt(size - i - 1)){
                color = red_color;
                isCompared = true;
            }

            normalized.setSpan(new BackgroundColorSpan(color), i, i + 1, 0);
            normalized.setSpan(new BackgroundColorSpan(color),size - 1 - i, size - i, 0);
            if( size % 2 == 1 && (i == size / 2 - 1) && isCompared == false)
                normalized.setSpan(new BackgroundColorSpan(color),size/2, size/2 + 1, 0);
            progress_bar.setProgress((i + 1) * 100/(size/2));

            activity.runOnUiThread( () -> textView1.setText(normalized) );

            if(isCompared) break;
            Thread.sleep(TIME/size);
        }
    }

    /**
     * Simple compare mode boolean.
     *
     * @param toCompare the to compare
     * @return the boolean
     */
    public boolean simpleCompareMode(String toCompare) {

        int size = toCompare.length();
        for (int i = 0; i < size;  i++) {
            if( toCompare.charAt(i) != toCompare.charAt(size - 1 - i)) {
                return false;
            }
        }

        return true;
    }

}
