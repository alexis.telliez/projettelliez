package projet.telliez;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.Button;

/**
 * The type About fragment.
 */
public class AboutFragment extends AlertDialog {

    private Button ok_button;

    /**
     * Instantiates a new About fragment.
     *
     * @param activity the activity
     */
    public AboutFragment(Activity activity) {
        super(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_about);
        ok_button = findViewById(R.id.ok_button);

        ok_button.setOnClickListener(v -> dismiss());
    }

}