package projet.telliez;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * The type Toast maker.
 */
public class ToastMaker {
    /**
     * Toast.
     *
     * @param msg     the msg
     * @param context the context
     */
    public static void toast(String msg, Context context){
        Toast toast = Toast.makeText(context,msg,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
